const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const productController = require("../Controllers/productController");

router.post("/add", auth.verify, productController.addProduct);
router.get("/viewall", productController.viewProducts);
router.get("/viewproduct", productController.viewProduct);
router.put("/update", auth.verify, productController.updateProduct);
router.put("/archive", auth.verify, productController.archiveProduct);

module.exports = router;